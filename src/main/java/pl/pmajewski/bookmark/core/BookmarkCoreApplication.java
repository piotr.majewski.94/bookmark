package pl.pmajewski.bookmark.core;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BookmarkCoreApplication {

    public static void main(String[] args) {
        SpringApplication.run(BookmarkCoreApplication.class, args);
    }

}
